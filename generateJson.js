#! /usr/bin/node
var things = `mine
mine_dangerous
mine_dark
mine_dark_dangerous
mine_desert
mine_desert_dangerous
mine_desert_dark
mine_desert_dark_dangerous
mine_dino
mine_frost
mine_frost_dangerous
mine_frost_dark
mine_frost_dark_dangerous
mine_lava
mine_lava_dangerous
mine_lava_dark
mine_lava_dark_dangerous
mine_quarryshaft
mine_slime
mine_slime_dangerous`.split("\n")

console.log(things.map(thing => `{
  "Action": "EditImage",
  "PatchMode": "Replace",
  "Target": "Maps/Mines/${thing}",
  "FromFile": "assets/Mines/${thing}.png"
}`).join(","))